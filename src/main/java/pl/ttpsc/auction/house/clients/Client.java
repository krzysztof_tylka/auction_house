package pl.ttpsc.auction.house.clients;

import org.springframework.stereotype.Component;
import pl.ttpsc.auction.house.entity.Entity;

@Component
public class Client extends Entity {

    private String name;
    private int age;
    private String email;

    public Client(String name, int age, String email) {
        this.name = name;
        this.age = age;
        this.email = email;
    }

    public Client() {
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getEmail() {
        return email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", email='" + email + '\'' +
                '}';
    }
}
