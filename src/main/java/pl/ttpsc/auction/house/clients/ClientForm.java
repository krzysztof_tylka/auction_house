package pl.ttpsc.auction.house.clients;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

public class ClientForm {

    @NotEmpty(message = "Name nie może być puste")
    private String name;

    @NotEmpty(message = "Email nie może być puste")
    @Email(message = "Email should be valid")
    private String email;

    //    @Range(min = 1, max = 4, message = "ID może zawirać maksymalnie 4 cyfry")
//    private int id;

    @Min(value = 18, message = "Age should not be less than 18")
    @Max(value = 150, message = "Age should not be greater than 150")
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "ClientForm{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                '}';
    }
}
