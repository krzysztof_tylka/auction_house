package pl.ttpsc.auction.house.clients;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class ClientService {

    @Autowired
    private ClientManager clientManager;

    public void add(ClientForm clientForm) {
        clientManager.add(new Client(clientForm.getName(), clientForm.getAge(), clientForm.getEmail()));
    }

    public void delete(Client client) {
        clientManager.delete(client);
    }

    public ClientForm changeClientToClientForm(int id) throws Exception {
        ClientForm clientForm = new ClientForm();
        Client client = clientManager.findClientById(id);
        clientForm.setName(client.getName());
        clientForm.setAge(client.getAge());
        clientForm.setEmail(client.getEmail());
        return clientForm;
    }

//    public Collection<String> getAllClientsToDisplay() {
//        Collection<String> allClientsToDisplay = new ArrayList<>();
//        for (Client client : clientManager.getAll()) {
//            allClientsToDisplay.add("Client id= " + client.getId() + ", name= " + client.getName() + ", age= " + client.getAge() + ", email" + client.getEmail());
//        }
//        return allClientsToDisplay;
//    }

    public Collection<Client> getAllClientsToDisplay() {
        Collection<Client> allClientsToDisplay = new ArrayList<>();
        for (Client client : clientManager.getAll()) {
            allClientsToDisplay.add(client);
        }
        return allClientsToDisplay;
    }

    public String getClientByIdToDisplay(int id) throws Exception {
        Client client = clientManager.findClientById(id);
        String clientById;
        if (client == null) {
            throw new Exception("There is no such a client in database");
        } else {
            clientById = "Client id=" + client.getId() + ", name=" + client.getName() + ", age=" + client.getAge();
        }
        return clientById;
    }

    public Client getClientById(int id) throws Exception {
        Client client = clientManager.findClientById(id);
        if (client == null) {
            throw new Exception("There is no such a client in database");
        } else {
            return client;
        }
    }

}
