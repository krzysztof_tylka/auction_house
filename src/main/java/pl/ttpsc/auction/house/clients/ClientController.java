package pl.ttpsc.auction.house.clients;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Collection;

@Controller
public class ClientController {

    @Autowired
    private ClientService clientService;

    @RequestMapping(value = "/clients/showClients", method = RequestMethod.GET)
    public String showClients(Model model) throws Exception {
        Collection<Client> clients = clientService.getAllClientsToDisplay();
        model.addAttribute("clients", clients);
        return "/clients/showClients";
    }

    @RequestMapping(value = "/clients/addClient", method = RequestMethod.GET)
    public String addClient(Model model) throws Exception {
        model.addAttribute("client", new ClientForm());
        return "/clients/addClient";
    }

    @RequestMapping(value = "/clients/addClient/{id}", method = RequestMethod.GET)
    public String addClientWithId(Model model, @PathVariable Integer id) throws Exception {
        ClientForm clientForm = clientService.changeClientToClientForm(id);
        model.addAttribute("client", clientForm);
        return "/clients/addClient";
    }

    @RequestMapping(value = "/clients/editClient/{id}", method = RequestMethod.GET)
    public String editClient(Model model, @PathVariable String id, ClientForm clientForm, BindingResult result) throws Exception {
        clientForm = clientService.changeClientToClientForm(Integer.parseInt(id));
        model.addAttribute("client", clientForm);
        model.addAttribute("clientId", id);
        return "/clients/savedClient";
    }

    @RequestMapping(value = "/clients/deleteClient/{id}", method = RequestMethod.GET)
    public String deleteClient(Model model, @PathVariable String id) throws Exception {
        model.addAttribute("id", id);
        clientService.delete(clientService.getClientById(Integer.parseInt(id)));
        return "/clients/deleteClient";
    }

    @RequestMapping(value = "/clients/deletedClient/{id}", method = RequestMethod.GET)
    public String deletedClient(Model model, @PathVariable String id) throws Exception {
        model.addAttribute("id", id);
        return "/clients/deletedClient";
    }


    @RequestMapping(value = "/clients/savedClient/{id}", method = RequestMethod.POST)
    public String editClientwWithId(
            Model model,
            @Valid @ModelAttribute("client") ClientForm clientForm,
            @PathVariable Integer id,
            BindingResult result) throws Exception {

        if (result.hasErrors()) {
            return "/clients/editClient";
        } else {
            model.addAttribute("id", id);
            Client client = clientService.getClientById(id);
            client.setName(clientForm.getName());
            client.setAge(clientForm.getAge());
            client.setEmail(clientForm.getEmail());
            return "/clients/savedClient";
        }
    }

    @RequestMapping(value = "/clients/addedClient", method = RequestMethod.POST)
    public String addedClient(@Valid @ModelAttribute("client") ClientForm clientForm, BindingResult
            result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("client", clientForm);
            return "/clients/addClient";
        }
        model.addAttribute("client", clientForm.toString());
        clientService.add(clientForm);
        return "/clients/addedClient";
    }
}
