package pl.ttpsc.auction.house.clients;

import org.springframework.stereotype.Repository;
import sun.security.krb5.internal.util.KrbDataInputStream;

import java.util.ArrayList;
import java.util.Collection;

@Repository
public class ClientManager {

    private Collection<Client> clientCollection = new ArrayList<>();
    static private int clientId = 0;

    ClientManager() {

        add(new Client("Asia",22, "asia@wp.pl" ));
        add(new Client("Jan",33, "jan@wp.pl" ));
        add(new Client("Kris",44, "kris@wp.pl" ));
        add(new Client("John",55, "john@wp.pl" ));
    }

    public void add(Client client) {
        client.setId(clientId);
        clientId++;
        clientCollection.add(client);
    }

    public void delete(Client client) {
        clientCollection.remove(client);
    }

    public Collection<Client> getAll() {
        return clientCollection;
    }

    public Client findClientById(int id) throws Exception {
        for (Client client : clientCollection) {
            if (client.getId() == id) {
                return client;
            }
        }
        throw new Exception("error");
    }

    public Collection<Client> findByName(String text) {
        Collection<Client> foundedClients = new ArrayList<>();
        for (Client client : clientCollection) {
            if (client.getName().toLowerCase().contains(text.toLowerCase())) {
                foundedClients.add(client);
            }
        }
        return foundedClients;
    }

}
