package pl.ttpsc.auction.house.clients;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class ClientRestController {

    @Autowired
    private ClientService clientService;

//    @RequestMapping("/clients/showClients")
//    public Collection<String> displayAllPersons() {
//        return clientService.getAllClientsToDisplay();
//    }

    @RequestMapping(value = "/findPersonById", method = RequestMethod.GET)
    public String displayPerson(@RequestParam int id) {
        try {
            return clientService.getClientByIdToDisplay(id);
        } catch (Exception e) {
            return e.getMessage();
        }
    }


}
