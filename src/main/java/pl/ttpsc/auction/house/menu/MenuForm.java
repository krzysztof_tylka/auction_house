package pl.ttpsc.auction.house.menu;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class MenuForm {

    @Min(value = 1, message = "Age should not be less than 18")
    @Max(value = 3, message = "Age should not be greater than 150")
    private int choice;

    public int getChoice() {
        return choice;
    }

    public void setChoice(int choice) {
        this.choice = choice;
    }
}
