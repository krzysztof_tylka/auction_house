package pl.ttpsc.auction.house.menu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MenuController {

    @Autowired
    private MenuService menuService;

    @RequestMapping("/")
    public String mainMenu(Model model) {
        return "/menu/menu";
    }

    @RequestMapping("/clients")
    public String clientMenu(Model model) {
        return "/menu/clients";
    }

    @RequestMapping("/products")
    public String productMenu(Model model) {
        return "/menu/products";
    }

    @RequestMapping("/auctions")
    public String auctionMenu(Model model) {
        return "/menu/auctions";
    }

}
