package pl.ttpsc.auction.house.entity;

public abstract class Entity {

    protected int id;

    public Entity() {
    }

    public int getId() {
        return id;
    }
}
