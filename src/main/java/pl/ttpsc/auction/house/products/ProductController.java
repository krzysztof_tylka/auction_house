package pl.ttpsc.auction.house.products;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
public class ProductController {


    @Autowired
    private ProductService productService;

    @RequestMapping("/allProducts")
    public Collection<String> displayAllProducts() {
        return productService.getAllProductsToDisplay();
    }

    @RequestMapping(value = "/findProductByName", method = RequestMethod.GET)
    public Collection<String> displayProductsByName (@RequestParam String name) {
            return productService.getProductsByNameToDisplay(name);
    }

}
