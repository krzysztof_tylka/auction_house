package pl.ttpsc.auction.house.products;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Collections;

@RestController
public class ProductRestController {

    @Autowired
    private ProductService productService;

//    @RequestMapping("/products")
//    public Collection<String> displayAllProdutsRestController() {
//        return productService.getAllProductsToDisplay();
//    }

    @RequestMapping(value = "/findProductById", method = RequestMethod.GET)
    public Collection<String> displayProductRestController(@RequestParam String name) {
        try {
            return productService.getProductsByNameToDisplay(name);
        } catch (Exception e) {
            return Collections.singleton(e.getMessage());
        }
    }


}
