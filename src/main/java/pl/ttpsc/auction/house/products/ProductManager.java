package pl.ttpsc.auction.house.products;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;

@Repository
public class ProductManager {

    ProductManager() {

//        add(new Product(1, "stół", "fajny dębowy stół", .findPersonById(1)));
    }

    private Collection<Product> productCollection = new ArrayList<>();

    public void add(Product product) {
        productCollection.add(product);
    }

    public Product findProductById(int id) {
        for (Product product : productCollection) {
            if (product.getId() == id) {
                return product;
            }
        }
        return null;
    }

    public Collection<Product> getAll() {
        return productCollection;
    }

    public Collection<Product> findByName(String text) {
        Collection<Product> foundedProducts = new ArrayList<>();
        for (Product product : productCollection) {
            if (product.getName().toLowerCase().contains(text.toLowerCase())) {
                foundedProducts.add(product);
            }
        }
        return foundedProducts;
    }

    public Collection<Product> getProductCollection() {
        return productCollection;
    }
}
