package pl.ttpsc.auction.house.products;

import org.springframework.stereotype.Component;
import pl.ttpsc.auction.house.clients.Client;

@Component
public class Product {

    private int id;
    private String name;
    private String description;
    private Client client;

    public Product (int id, String name, String description, Client client) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.client = client;
    }

    public Product() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Client getClient() {
        return client;
    }

}
