package pl.ttpsc.auction.house.products;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

@Service
public class ProductService {

    @Autowired
    private ProductManager productManager;

    public Collection<String> getAllProductsToDisplay() {
        Collection<String> allProducts = new ArrayList<>();
        for (Product product : productManager.getAll()) {
            allProducts.add("Product id= " + product.getId() + ", name= " + product.getName() + ", description= " + product.getDescription());
        }
        return allProducts;
    }

    public Collection<String> getProductsByNameToDisplay(String name) {
        Collection<String> allProductsByName = new ArrayList<>();
        for (Product product : productManager.getAll()) {
            if (product.getName().toLowerCase().contains(name.toLowerCase())) {
                allProductsByName.add("Product id= "+product.getId()+", name= "+product.getName()+", description= "+product.getDescription()+ "which belongs to= "+product.getClient());
            }
        }
        return allProductsByName;
    }
}
