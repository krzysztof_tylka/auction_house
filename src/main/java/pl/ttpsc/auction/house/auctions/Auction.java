package pl.ttpsc.auction.house.auctions;

import org.springframework.stereotype.Component;
import pl.ttpsc.auction.house.products.Product;

import java.util.Calendar;

@Component
public class Auction {

    private Integer id;
    private float price;
    private Calendar dateOfStart;
    private Calendar dateOfEnd;
    private Product product;

    public Auction(Integer id, float price, Calendar dateOfStart, Calendar dateOfEnd, Product product) {
        this.id = id;
        this.price = price;
        this.dateOfStart = dateOfStart;
        this.dateOfEnd = dateOfEnd;
        this.product = product;
    }

    public Auction() {
    }

    public int getId() {
        return id;
    }

    public float getPrice() {
        return price;
    }

    public Calendar getDateOfStart() {
        return dateOfStart;
    }

    public Calendar getDateOfEnd() {
        return dateOfEnd;
    }

    public Product getProduct() {
        return product;
    }
}
