package pl.ttpsc.auction.house.auctions;

import org.springframework.stereotype.Repository;
import pl.ttpsc.auction.house.auctions.Auction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;

@Repository
public class AuctionManager {

    private Collection<Auction> auctionCollection = new ArrayList<>();

    public void add(Auction auction) {
        auctionCollection.add(auction);
    }

    public Collection<Auction> getAll() {
        return auctionCollection;
    }

    public Auction getAuctionById(int id) {
        for (Auction auction : auctionCollection) {
            if (auction.getId() == id) {
                return auction;
            }
        }
        return null;
    }

    public Collection<Auction> getActive() {
        Collection<Auction> activeAuctions = new ArrayList<>();
        for (Auction auction : auctionCollection) {
            if (auction.getDateOfStart().before(Calendar.getInstance()) && auction.getDateOfEnd().after(Calendar.getInstance())) {
                activeAuctions.add(auction);
            }
        }
        return activeAuctions;
    }

}
